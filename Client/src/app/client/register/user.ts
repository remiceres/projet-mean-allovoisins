export class User {
  constructor(
    public last_name: string,
    public first_name: string,
    public email: string,
    public password: string,
    public address: string,
    public city: string,
    public phone: string,
    public birthday: string,
    public role: string,
    public register_date: string,
    public avatar: string,
    public sell: number,
    public buy:number,
    public ratio: number,
  ) {}
}

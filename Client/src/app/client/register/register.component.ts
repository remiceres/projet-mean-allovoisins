import { Component } from '@angular/core';
import { ClientService } from '../client.service';
import { User } from './user';
import {Router} from '@angular/router';
import { AuthServiceService} from '../../auth/auth-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

    private succesMessage;
    private errorMessage;

    constructor(private service:ClientService, private authService:AuthServiceService, private router: Router) { }

    private today = new Date();
    private dateString = this.today.getDate() + '/'
        + (this.today.getMonth()+1) + '/'
        + this.today.getFullYear();

    onSubmit(value): void {
        this.succesMessage = ""
        this.errorMessage = ""
        let user = new User(value.last_name, value.first_name, value.email, value.password, value.address, value.city, value.phone, value.birthday, 'user', this.dateString, value.avatar,0,0, 0);
        this.service.addUser(user).subscribe(
            res =>{
                console.log("succes : " + res);
                this.succesMessage = res.message

                this.authService.userConnectedEmail = value.email;
                this.authService.userConnectedRole = 'user';
                this.authService.userConnectedAvatar = value.avatar;
                this.authService.userConnectedFirstName = value.first_name;
                this.authService.userConnectedLastName = value.last_name;

                this.router.navigate( ['/' ]);
            },
            err => {
                console.log(err);
                this.errorMessage = err.error.message
            }

        )
    }
}

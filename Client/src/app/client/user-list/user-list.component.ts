import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

    private members : Object[];

    constructor(private service:ClientService) { }

    ngOnInit() {
        this.service.getMembers().subscribe(res => {
            this.members = res;
        }
        );
    }

}

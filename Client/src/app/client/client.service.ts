import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class ClientService {
    private pathServeur='http://localhost:8888/';
    constructor(private http:HttpClient) { }

    getMembers():Observable<any> {
        return this.http.get(this.pathServeur + 'getUsers');
    }

    addUser(userInfo):Observable<any> {
        const body = JSON.stringify(userInfo);
        return this.http.post(this.pathServeur + 'addUser', body, httpOptions);
    }

    getMembersByName(name,trie):Observable<any> {
        const body = JSON.stringify({'name':name, 'trie':trie});
        console.log(body);
        var path = this.pathServeur + 'getUsersByName/';
        return this.http.post(path,body,httpOptions);
    }

    getMembersByID(email):Observable<any> {
        var path = this.pathServeur + 'getUsersByID/' + email;
        return this.http.get(path);
    }

    removeMembersByID(email):Observable<any> {
        var path = this.pathServeur + 'removeUsersByID/' + email;
        return this.http.get(path);
    }

    changeParamsUser(email, obj):Observable<any> {
      const body = JSON.stringify(obj);
      console.log(body);
      var path = this.pathServeur + 'changeParamsUser/' + email;
      return this.http.post(path,body,httpOptions);
    }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAffichageComponent } from './user-affichage.component';

describe('UserAffichageComponent', () => {
  let component: UserAffichageComponent;
  let fixture: ComponentFixture<UserAffichageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAffichageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAffichageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

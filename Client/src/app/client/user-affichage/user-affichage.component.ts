import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router'
import {Router} from '@angular/router';
import { AuthServiceService } from '../../auth/auth-service.service';
import { ClientService } from '../../client/client.service';
import { BienServiceService } from '../../bien-service/bien-service.service';
import { AdminService} from '../../admin/admin.service';

@Component({
  selector: 'app-user-affichage',
  templateUrl: './user-affichage.component.html',
  styleUrls: ['./user-affichage.component.css']
})
export class UserAffichageComponent implements OnInit {

    private email: string;
    private users : Object[];
    private last_name: string;
    private first_name: string;
    private password: string;
    private address: string;
    private city: string;
    private phone: string;
    private birthday: string;
    private role: string;
    private register_date: string;
    private avatar: string;
    private sell: string;
    private buy: string;
    private ratio: string;

    private propertysAvailable : Object[] = [];
    private servicesAvailable : Object[] = [];
    private propertysUnavailable : Object[] = [];
    private servicesUnavailable : Object[] = [];

    constructor(private adminService:AdminService, private serviceBienServ:BienServiceService, private serviceAuth:AuthServiceService, private service:ClientService, private router: Router, private route: ActivatedRoute) { }

    ngOnInit() {
        this.email=this.route.snapshot.params['email'];
        this.service.getMembersByID(this.email).subscribe(res => {
            this.users=res;
            this.last_name  = res[0].last_name;
            this.first_name = res[0].first_name;
            this.password = res[0].password;
            this.address = res[0].address;
            this.city = res[0].city;
            this.phone = res[0].phone;
            this.birthday = res[0].birthday;
            this.role = res[0].role;
            this.register_date = res[0].register_date;
            this.avatar = res[0].avatar;
            this.sell = res[0].sell;
            this.buy = res[0].buy;
            this.ratio = res[0].ratio;
            console.log(this.first_name);
        });

        this.serviceBienServ.getBienServiceUser(this.email).subscribe(res => {
            this.propertysAvailable = res["propertysAvailable"];
            this.servicesAvailable = res["servicesAvailable"];
            this.propertysUnavailable = res["propertysUnavailable"];
            this.servicesUnavailable = res["servicesUnavailable"];
        });
    }

    remove() {
      this.service.removeMembersByID(this.email).subscribe(
          res => {
            this.adminService.userRatioLimit().subscribe(res => {
              if(res.length>0){
                this.adminService.limitUser=true;
              }
              else {
                  this.adminService.limitUser=false;
              }
            });
            this.router.navigate( ['/admin' ]);
          });
    }

    change(value) {
        var image = value.image;
        if(value.image === undefined || value.image === ''){
          image=this.avatar;
        }
        this.avatar=image;
        var num = value.num;
        if(value.num === undefined || value.num === ''){
          num=this.phone;
        }
        this.phone=num;
        var addr = value.addr;
        if(value.addr === undefined || value.addr === ''){
          addr=this.address;
        }
        this.address=addr;
        var ville = value.ville;
        if(value.ville === undefined || value.ville === ''){
          ville=this.city;
        }
        this.city=ville;

        this.service.changeParamsUser(this.email,{'address':addr, 'city':ville, 'phone':num, 'avatar':image}).subscribe(res => {
            console.log('changer ! ');
        });
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { UserListComponent } from './user-list/user-list.component';
import { RegisterComponent } from './register/register.component';
import { UserSearchComponent } from './user-search/user-search.component';
import { UserAffichageComponent } from './user-affichage/user-affichage.component';
import { RouterModule } from '@angular/router';
import { BeteModule } from '../bete/bete.module';

@NgModule({
  declarations: [UserListComponent, RegisterComponent, UserSearchComponent, UserAffichageComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    RouterModule,
    BeteModule
  ],
  exports: [UserListComponent, RegisterComponent, UserSearchComponent, UserAffichageComponent]
})
export class ClientModule { }

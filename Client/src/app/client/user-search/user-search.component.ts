import { Component } from '@angular/core';
import { ClientService } from '../client.service';

@Component({
  selector: 'app-user-search',
  templateUrl: './user-search.component.html',
  styleUrls: ['./user-search.component.css']
})
export class UserSearchComponent {

    constructor(private service:ClientService) { }

    private members : Object[];
    private notFound : boolean = false

    onSubmit(value): void {
        this.members = []
        this.notFound = false
        if (value.search === undefined)
        {
            value.search='';
        }
        else
        {
            var inv=1;
            if(value.inv===true){
              inv=-1;
            }
            var trie={};
            if(value.trie === 'ratio'){
              trie={'ratio':inv};
            }
            if(value.trie === 'sell'){
              trie={'sell':inv};
            }
            if(value.trie === 'buy'){
              trie={'buy':inv};
            }
            if(value.trie==='inscritpion'){
              trie={'register_date':inv};
            }
            if(value.trie==='nomPrenom'){
              trie={'last_name':inv,'first_name':inv};
            }
            console.log(trie);
            this.service.getMembersByName(value.search,trie).subscribe(
                res => {
                    console.log(res);
                    if (res.length === 0)
                    {
                        this.notFound = true;
                    }
                    this.members = res;
                }
            );
        }
    }
}

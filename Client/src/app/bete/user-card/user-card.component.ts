import { Component, Input, OnInit} from '@angular/core';
import { ClientService } from '../../client/client.service';
import { AuthServiceService} from '../../auth/auth-service.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.css']
})
export class UserCardComponent implements OnInit {
    @Input() email: string;
    constructor(private router: Router, private service:ClientService, private AuthService:AuthServiceService) { }

    private users : Object[];

    @Input() avatar : string;
    @Input() first_name : string;
    @Input() last_name : string;
    @Input() desc : string;
    @Input() address : string;
    @Input() city : string;
    @Input() phone : string;
    @Input() ratio : string;
    @Input() sell : string;
    @Input() buy : string;
    @Input() register_date : string;
    @Input() role:string;

    ngOnInit() {
        if(this.first_name === undefined){
          this.service.getMembersByID(this.email).subscribe(
              res => {
                  this.users=res[0];
                  this.avatar = res[0].avatar;
                  this.first_name = res[0].first_name;
                  this.last_name = res[0].last_name;
                  this.desc = res[0].desc;
                  this.address = res[0].address;
                  this.city = res[0].city;
                  this.phone = res[0].phone;
                  this.ratio = res[0].ratio;
                  this.sell = res[0].sell;
                  this.buy = res[0].buy;
                  this.register_date = res[0].register_date;
                  this.role=res[0].role;
              },
              err => {
                  console.log("err : ")
                  console.log(err);
              }
            );
        }

    }
}

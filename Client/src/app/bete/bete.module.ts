import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserCardComponent } from './user-card/user-card.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BienServiceCardComponent } from './bien-service-card/bien-service-card.component';
import {KeyWordModule} from '../key-word/key-word.module';
import {AvailabilityModule} from '../availability/availability.module';

@NgModule({
  declarations: [UserCardComponent, BienServiceCardComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    KeyWordModule,
    AvailabilityModule
  ],
  exports: [UserCardComponent, BienServiceCardComponent]
})
export class BeteModule { }

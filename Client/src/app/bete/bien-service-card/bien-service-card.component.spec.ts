import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BienServiceCardComponent } from './bien-service-card.component';

describe('BienServiceCardComponent', () => {
  let component: BienServiceCardComponent;
  let fixture: ComponentFixture<BienServiceCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BienServiceCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BienServiceCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

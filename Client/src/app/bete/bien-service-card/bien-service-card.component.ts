import { Component, Input, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { BienServiceService } from '../../bien-service/bien-service.service';
import {ActivatedRoute} from '@angular/router'


@Component({
  selector: 'app-bien-service-card',
  templateUrl: './bien-service-card.component.html',
  styleUrls: ['./bien-service-card.component.css']
})
export class BienServiceCardComponent implements OnInit {
    @Input() id: string;
    @Input() type: string;

    private money : string;
    private products : Object[];
    private typeFr : string;
    private productPrint = 0;
    private emailMembre: string;
    private name: string;
    private email: string;
    private desc: string;
    private price: string;
    private picture: string;

    constructor(private router: Router, private service:BienServiceService, private route: ActivatedRoute) {}

    ngOnInit() {
      this.id=this.id;
      this.type=this.type;

      if (this.type === "Propertys")
      {
          this.money = "€"
          this.typeFr = "Bien"
      }
      else
      {
          this.money = "€/h"
          this.typeFr = "Service"
      }

      this.service.getSingleProduct(this.id, this.type).subscribe(res => {

        this.products=res;
        this.productPrint  = res[0].product;
        this.emailMembre = res[0].emailMembre;
        this.name = res[0].name;
        this.email = res[0].email;
        this.desc = res[0].desc;
        this.price = res[0].price;
        this.picture = res[0].picture;
      });
    }
}

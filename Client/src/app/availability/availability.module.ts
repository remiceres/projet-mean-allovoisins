import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AvailabilityAffichageComponent } from './availability-affichage/availability-affichage.component';
import { AvailabilityDisponibleComponent } from './availability-disponible/availability-disponible.component';
import { AvailabilityIndisponibleComponent } from './availability-indisponible/availability-indisponible.component';
import { AvailabilityVideComponent } from './availability-vide/availability-vide.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [AvailabilityAffichageComponent, AvailabilityDisponibleComponent, AvailabilityIndisponibleComponent, AvailabilityVideComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    RouterModule
  ],
  exports: [AvailabilityAffichageComponent, AvailabilityDisponibleComponent, AvailabilityIndisponibleComponent, AvailabilityVideComponent]
})
export class AvailabilityModule { }

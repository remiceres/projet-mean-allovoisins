import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailabilityVideComponent } from './availability-vide.component';

describe('AvailabilityVideComponent', () => {
  let component: AvailabilityVideComponent;
  let fixture: ComponentFixture<AvailabilityVideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailabilityVideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailabilityVideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

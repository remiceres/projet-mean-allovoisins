import { Component, Input } from '@angular/core';
import {AvailabilityService} from '../availability.service';
import { AuthServiceService } from '../../auth/auth-service.service';
@Component({
  selector: 'app-availability-vide',
  templateUrl: './availability-vide.component.html',
  styleUrls: ['./availability-vide.component.css']
})
export class AvailabilityVideComponent{

  @Input() id: string;
  @Input() type: string;
  @Input() owner: string;
  @Input() date: string;
  @Input() AMPM: string;
  constructor(private service:AvailabilityService, private serviceAuth:AuthServiceService) { }

  submitAvaibility() {
    this.service.submitAvaibility(this.id, this.type, this.date, this.AMPM).subscribe(res => {
      console.log(res);
    });
  }


}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailabilityDisponibleComponent } from './availability-disponible.component';

describe('AvailabilityDisponibleComponent', () => {
  let component: AvailabilityDisponibleComponent;
  let fixture: ComponentFixture<AvailabilityDisponibleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailabilityDisponibleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailabilityDisponibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

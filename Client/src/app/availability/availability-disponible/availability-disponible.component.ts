import { Component, Input, OnInit } from '@angular/core';
import {AvailabilityService} from '../availability.service';
import { AuthServiceService } from '../../auth/auth-service.service';

@Component({
  selector: 'app-availability-disponible',
  templateUrl: './availability-disponible.component.html',
  styleUrls: ['./availability-disponible.component.css']
})
export class AvailabilityDisponibleComponent implements OnInit {

  @Input() id: string;
  @Input() type: string;
  @Input() owner: string;
  @Input() date: string;
  @Input() AMPM: string;
  private take = false;

  constructor(private service:AvailabilityService, private serviceAuth:AuthServiceService) { }

  ngOnInit() {
    if(this.owner!==this.serviceAuth.userConnectedEmail)
      {this.service.takeVerif(this.date,this.AMPM, this.serviceAuth.userConnectedEmail).subscribe(res => {
        console.log(res);
        if(res.length > 0){
          this.take = true;
        }
      });}
  }

  submitUtilization() {
    this.service.submitUtilization(this.id, this.type, this.date, this.AMPM, this.serviceAuth.userConnectedEmail);
  }

  deleteAvaibility() {
    this.service.deleteAvaibility(this.id, this.type, this.date, this.AMPM).subscribe(res => {
      console.log(res);
    });
  }

}

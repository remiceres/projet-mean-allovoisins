export class CalendarDate {
  constructor(
    public date: string,
    private day:string,
    public AM: string,
    public PM: string
  ) {}
}

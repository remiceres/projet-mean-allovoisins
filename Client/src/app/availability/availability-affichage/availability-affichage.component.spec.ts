import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailabilityAffichageComponent } from './availability-affichage.component';

describe('AvailabilityAffichageComponent', () => {
  let component: AvailabilityAffichageComponent;
  let fixture: ComponentFixture<AvailabilityAffichageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailabilityAffichageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailabilityAffichageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

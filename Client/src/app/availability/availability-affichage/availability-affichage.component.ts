import { Component, OnInit, Input } from '@angular/core';
import {AvailabilityService} from '../availability.service';
import {CalendarDate} from '../CalendarDate';

@Component({
  selector: 'app-availability-affichage',
  templateUrl: './availability-affichage.component.html',
  styleUrls: ['./availability-affichage.component.css']
})
export class AvailabilityAffichageComponent implements OnInit {

  @Input() id: string;
  @Input() type: string;
  @Input() owner: string;
  @Input() price: string;
  constructor(private service:AvailabilityService) { }

  getDate(num):string {
    var today= new Date();
    var date=new Date(today.setDate(today.getDate()+num));
    var day=date.getDate();
    var mouth=date.getMonth()+1;
    var year=date.getFullYear();
    return year + '-' + mouth + '-' + day;
  }

  getCalendar() {
    this.service.calendar = [];
    const today= new Date();
    var decalage=today.getDay();
    var k=0;
    if( this.service.week === 0 ){
      k = today.getDay();
      decalage=0;
    }
    var dateDeb=this.getDate((7*this.service.week)-decalage);
    var dateFin=this.getDate(6-k+(7*this.service.week)-decalage);

    //On demande au service les dispoibilité et indisponibilité du produit
    this.service.getAvailability(this.id, this.type, dateDeb, dateFin).subscribe(res => {
      console.log(res);

      for(var i=0 ; i<7; i++){
        if(i<k){
          this.service.calendar.push(new CalendarDate(this.getDate(i-k),this.service.days[i], '-1', '-1'));
        }
        else{
          const datei = this.getDate(i-k+(7*this.service.week)-decalage);
          var stateAM = '0';
          if (res['availability'].includes(datei + '/AM')) {
            stateAM = '1';
          }
          if (res['utilization'].includes(datei + '/AM')) {
            stateAM = '2';
          }

          var statePM = '0';
          if (res['availability'].includes(datei + '/PM')) {
            statePM = '1';
          }
          if (res['utilization'].includes(datei + '/PM')) {
            statePM = '2';
          }

          this.service.calendar.push(new CalendarDate(datei,this.service.days[i], stateAM, statePM));
        }
      }
    });
  }

  ngOnInit() {
    this.service.buy=[];
    this.service.week = 0;
    this.getCalendar();
  }

  afterWeek() {
    this.service.week=(this.service.week+1);
    this.getCalendar();
  }

  beforeWeek() {
    if(this.service.week > 0){
      this.service.week=(this.service.week-1);
      this.getCalendar();
    }
  }

  submitBuy(){
    this.service.submitBuy().subscribe(res => {
      console.log(res);
    });
  }

}

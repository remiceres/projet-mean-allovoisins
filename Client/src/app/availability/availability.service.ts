import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import {CalendarDate} from './CalendarDate';
import {Product} from './Prducts';


const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AvailabilityService {

  public calendar : CalendarDate[] = [];
  public days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
  public week;
  private pathServeur='http://localhost:8888/';
  public buy: Product[] = [];

  constructor(private http:HttpClient) { }

  getAvailability(id,type,dateDeb, dateFin):Observable<any> {
    var path = this.pathServeur + 'availability/' + id + '/' + type + '/' + dateDeb + '/' + dateFin;
    return this.http.get(path);
  }

  getUser(id, type, date, AMPM):Observable<any> {
    var path = this.pathServeur + 'getUserUtilization/' + id + '/' + type + '/' + date + '/' + AMPM;
    return this.http.get(path);
  }

  submitAvaibility(id, type, date, AMPM):Observable<any> {
    var path = this.pathServeur + 'addAvaibility/' + id + '/' + type + '/' + date + '/' + AMPM;
    const d = this.calendar.map(x => x.date);
    const index = d.indexOf(date);
    if (index > -1) {
      if (AMPM === 'AM') {
        this.calendar[index].AM = '1';
      }
      if (AMPM === 'PM') {
        this.calendar[index].PM = '1';
      }
      return this.http.get(path);
    }
  }

  deleteAvaibility(id, type, date, AMPM):Observable<any> {
    var path = this.pathServeur + 'removeAvaibility/' + id + '/' + type + '/' + date + '/' + AMPM;
    const d = this.calendar.map(x => x.date);
    const index = d.indexOf(date);
    if (index > -1) {
      if (AMPM === 'AM') {
        this.calendar[index].AM = '0';
      }
      if (AMPM === 'PM') {
        this.calendar[index].PM = '0';
      }
      return this.http.get(path);
    }
  }

  submitUtilization(id, type, date, AMPM, user) {
    const d = this.calendar.map(x => x.date);
    const index = d.indexOf(date);
    if (index > -1) {
      this.buy.push(new Product(id, type, date, AMPM, user));
      if (AMPM === 'AM') {
        this.calendar[index].AM = '2';
      }
      if (AMPM === 'PM') {
        this.calendar[index].PM = '2';
      }
    }
  }

  submitBuy():Observable<any>{
    var path = this.pathServeur + 'addUtilization';
    const body = JSON.stringify(this.buy);
    console.log(path + ', ' + body);
    this.buy=[];
    return this.http.post(path, body, httpOptions);
  }

  deleteUtilization(id, type, date, AMPM, user):Observable<any> {
    const d = this.calendar.map(x => x.date);
    const index = d.indexOf(date);
    if (index > -1) {
      if (AMPM === 'AM') {
        this.calendar[index].AM = '1';
      }
      if (AMPM === 'PM') {
        this.calendar[index].PM = '1';
      }
      var i=0
      for(; i < this.buy.length; i++){
        if(this.buy[i].date === date && this.buy[i].AMPM === AMPM){
          this.buy.splice(i,1);
        }
      }
      if(i==this.buy.length){
        var path = this.pathServeur + 'removeUtilization/' + id + '/' + type + '/' + date + '/' + AMPM + '/' + user;
        return this.http.get(path);
      }
    }

  }

  takeVerif(date, AMPM, email):Observable<any> {
    var path = this.pathServeur + 'verifUse/' + date + '/' + AMPM + '/' + email;
    return this.http.get(path);
  }
}

import { Component, OnInit, Input } from '@angular/core';
import {AvailabilityService} from '../availability.service';
import { AuthServiceService } from '../../auth/auth-service.service';
import {Product} from '../Prducts';

@Component({
  selector: 'app-availability-indisponible',
  templateUrl: './availability-indisponible.component.html',
  styleUrls: ['./availability-indisponible.component.css']
})
export class AvailabilityIndisponibleComponent implements OnInit {

  @Input() id: string;
  @Input() type: string;
  @Input() owner: string;
  @Input() date: string;
  @Input() AMPM: string;
  private user : string;
  constructor(private service:AvailabilityService, private serviceAuth:AuthServiceService) { }

  ngOnInit() {
    //Récupérer user qui l'utilise
    this.user = '';
    if(this.service.buy.length>0){
      this.user=this.serviceAuth.userConnectedEmail.toString();
    }
    else{
      this.service.getUser(this.id, this.type, this.date, this.AMPM).subscribe(res => {
        console.log(res);
        if(res.length>0){
          this.user=res[0].mailUser;
          console.log(res[0].mailUser);
        }
      });
    }
  }

  deleteUtilization() {
    this.service.deleteUtilization(this.id, this.type, this.date, this.AMPM, this.user).subscribe(res => {
      console.log(res);
    });
  }

}

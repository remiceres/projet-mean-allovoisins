import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailabilityIndisponibleComponent } from './availability-indisponible.component';

describe('AvailabilityIndisponibleComponent', () => {
  let component: AvailabilityIndisponibleComponent;
  let fixture: ComponentFixture<AvailabilityIndisponibleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailabilityIndisponibleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailabilityIndisponibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

export class Product {
  constructor(
    public id: string,
    public type:string,
    public date:string,
    public AMPM: string,
    public user: String
  ) {}
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AuthUserComponent } from './auth-user/auth-user.component';
import {NavigationModule} from '../navigation/navigation.module'
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [AuthUserComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    RouterModule,
    NavigationModule
  ],
  exports: [AuthUserComponent]
})
export class AuthModule { }

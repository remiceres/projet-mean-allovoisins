import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  public userConnectedEmail : String = '';
  public userConnectedRole : String = '';
  public userConnectedAvatar : String = '';
  public userConnectedFirstName : String = '';
  public userConnectedLastName : String = '';



  private pathServeur='http://localhost:8888/';

  constructor(private http:HttpClient) { }

  login(email, password):Observable<any> {
    return this.http.get(this.pathServeur + 'authentification/' + email + "/" + password);
  }
}

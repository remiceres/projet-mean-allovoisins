import { Component } from '@angular/core';
import { AuthServiceService } from '../auth-service.service';
import {Router} from '@angular/router';
import { AdminService} from '../../admin/admin.service';

@Component({
  selector: 'app-auth-user',
  templateUrl: './auth-user.component.html',
  styleUrls: ['./auth-user.component.css']
})
export class AuthUserComponent {

  constructor(private service:AuthServiceService, private router: Router, private serviceAdmin:AdminService) { }

  private errorMessage;

  onSubmit(value): void {
    this.errorMessage = "";
    console.log(value.email);
    this.service.login(value.email, value.password).subscribe(
      res =>{
          console.log("succes : " + res[0].email);
          this.service.userConnectedEmail = res[0].email;
          this.service.userConnectedRole = res[0].role;
          this.service.userConnectedAvatar = res[0].avatar;
          this.service.userConnectedFirstName = res[0].first_name;
          this.service.userConnectedLastName = res[0].last_name;
          if(res[0].role==='admin'){
            this.serviceAdmin.userRatioLimit().subscribe(res => {
              console.log(res);
              if(res.length>0){
                this.serviceAdmin.limitUser=true;
              }
            });
          }
          this.router.navigate( ['/' ]);
      },
      err => {
          console.log(err);
          this.errorMessage = err.error.message;
      })
  }
}

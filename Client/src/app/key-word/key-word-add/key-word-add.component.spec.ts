import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyWordAddComponent } from './key-word-add.component';

describe('KeyWordAddComponent', () => {
  let component: KeyWordAddComponent;
  let fixture: ComponentFixture<KeyWordAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeyWordAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyWordAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

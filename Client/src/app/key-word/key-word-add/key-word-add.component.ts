import { Component, OnInit, Input } from '@angular/core';
import { KeyWordService} from '../key-word.service';
import { Keyword } from '.././Keyword';


@Component({
  selector: 'app-key-word-add',
  templateUrl: './key-word-add.component.html',
  styleUrls: ['./key-word-add.component.css']
})
export class KeyWordAddComponent implements OnInit {

  @Input() id: string;
  @Input() type: string;
  private keyWordPropertys : Object[] = [];
  private keyWordServices : Object[] = [];
  private errorMessage;
  private keyWord;
  
  constructor(private service:KeyWordService) { }

  ngOnInit() {
    this.service.getAllKeyWord().subscribe(res => {
      console.log(res);
      this.keyWordPropertys = res["propertys"];
      this.keyWordServices = res["services"];
    });
  }

  submitKeyWord(value): void {
     this.errorMessage = "";
     this.keyWord = "";
    console.log("mots clef ajouté " + value.keyWord);
    if( value.keyWord !== undefined ){
      this.service.addKeyWord(this.id, this.type, value.keyWord).subscribe(
          res => {},
          err => {this.errorMessage = err.error.message}
      )
    }
  }

}

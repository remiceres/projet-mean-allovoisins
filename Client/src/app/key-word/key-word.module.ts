import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { KeyWordAfficheComponent } from './key-word-affiche/key-word-affiche.component';
import { KeyWordSupprimableComponent } from './key-word-supprimable/key-word-supprimable.component';
import { KeyWordAddComponent } from './key-word-add/key-word-add.component';

@NgModule({
  declarations: [KeyWordAfficheComponent, KeyWordSupprimableComponent, KeyWordAddComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
  ],
  exports: [KeyWordAfficheComponent, KeyWordSupprimableComponent, KeyWordAddComponent]
})
export class KeyWordModule { }

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Keyword } from './Keyword';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class KeyWordService {

  public listKeyWord : Keyword[] = [];
  private pathServeur='http://localhost:8888/';
  constructor(private http:HttpClient) { }

  getAllKeyWord():Observable<any> {
    var path = this.pathServeur + 'getKeyWord/';
    return this.http.get(path);
  }

  getKeyWord(id, type):Observable<any> {
    var path = this.pathServeur + 'getKeyWord/' + id + '/' + type;
    return this.http.get(path);
  }

  removeKeyWord(id, type, keyword):Observable<any> {
    var path = this.pathServeur + 'removeKeyWord/' + id + '/' + type + '/' +keyword;
    console.log(id + ',' + keyword);
    const idOf = this.listKeyWord.map(x => x.keyword);
    const index = idOf.indexOf(keyword);
    console.log(index);
    if (index > -1) {
        this.listKeyWord.splice(index, 1);
    }
    return this.http.get(path);
  }

  addKeyWord(id, type, keyword):Observable<any> {
    const body = new Keyword(id,keyword);
    const desc = this.listKeyWord.map(x => x.keyword);
    if(!desc.includes(keyword) && keyword!=''){
      this.listKeyWord.push(body);
    }
    return this.http.post(this.pathServeur + 'addKeyWord/' + type, body, httpOptions);

  }
}

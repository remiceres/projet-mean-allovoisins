import { Component, OnInit, Input } from '@angular/core';
import { KeyWordService} from '../key-word.service';
import { AuthServiceService } from '../../auth/auth-service.service';

@Component({
  selector: 'app-key-word-affiche',
  templateUrl: './key-word-affiche.component.html',
  styleUrls: ['./key-word-affiche.component.css']
})
export class KeyWordAfficheComponent implements OnInit {
  @Input() id: string;
  @Input() type: string;
  @Input() owner: string;

  constructor(private service:KeyWordService, private serviceAuth:AuthServiceService) { }

  ngOnInit() {
    this.service.listKeyWord = [];

    this.service.getKeyWord(this.id, this.type).subscribe(res => {
      console.log(res);
      this.service.listKeyWord = res;
    });
  }

}

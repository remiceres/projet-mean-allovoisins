import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyWordAfficheComponent } from './key-word-affiche.component';

describe('KeyWordAfficheComponent', () => {
  let component: KeyWordAfficheComponent;
  let fixture: ComponentFixture<KeyWordAfficheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeyWordAfficheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyWordAfficheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

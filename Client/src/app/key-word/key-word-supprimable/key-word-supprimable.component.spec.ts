import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyWordSupprimableComponent } from './key-word-supprimable.component';

describe('KeyWordSupprimableComponent', () => {
  let component: KeyWordSupprimableComponent;
  let fixture: ComponentFixture<KeyWordSupprimableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeyWordSupprimableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyWordSupprimableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

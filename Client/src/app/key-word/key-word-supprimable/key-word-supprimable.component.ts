import { Component, Input } from '@angular/core';
import { KeyWordService} from '../key-word.service';

@Component({
  selector: 'app-key-word-supprimable',
  templateUrl: './key-word-supprimable.component.html',
  styleUrls: ['./key-word-supprimable.component.css']
})
export class KeyWordSupprimableComponent {

  @Input() id: string;
  @Input() type: string;
  @Input() keyword : string;

  constructor(private service:KeyWordService) { }

  removeKeyWord(): void {
    console.log('remove : ' + this.id + ', ' + this.type)
    this.service.removeKeyWord(this.id, this.type, this.keyword).subscribe(res => {
        console.log(res);
    }
    );
  }

}

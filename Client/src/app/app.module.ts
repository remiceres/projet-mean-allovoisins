import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientModule } from './client/client.module';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BienServiceModule } from './bien-service/bien-service.module';
import { RouterModule } from '@angular/router';
import { AuthModule } from './auth/auth.module';
import { NavigationModule } from './navigation/navigation.module';
import { KeyWordModule } from './key-word/key-word.module';
import { AvailabilityModule } from './availability/availability.module';
import { AdminModule } from './admin/admin.module';
import { BeteModule } from './bete/bete.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ClientModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BienServiceModule,
    RouterModule,
    AuthModule,
    NavigationModule,
    KeyWordModule,
    AvailabilityModule,
    AdminModule,
    BeteModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

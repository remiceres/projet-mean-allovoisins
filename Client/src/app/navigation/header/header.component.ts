import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../../auth/auth-service.service';
import { AdminService} from '../../admin/admin.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router, private service:AuthServiceService, private serviceAuth:AuthServiceService, private adminService:AdminService) { }

  ngOnInit() {}

  logout(): void {
    this.service.userConnectedEmail = '';
    this.service.userConnectedRole = '';
    this.service.userConnectedAvatar = '';
    this.service.userConnectedFirstName = '';
    this.service.userConnectedLastName = '';
    this.router.navigate( ['/auth' ]);
  }

}

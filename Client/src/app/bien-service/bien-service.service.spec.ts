import { TestBed, inject } from '@angular/core/testing';

import { BienServiceService } from './bien-service.service';

describe('BienServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BienServiceService]
    });
  });

  it('should be created', inject([BienServiceService], (service: BienServiceService) => {
    expect(service).toBeTruthy();
  }));
});

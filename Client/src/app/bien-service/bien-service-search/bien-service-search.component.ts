import { Component, OnInit} from '@angular/core';
import { BienServiceService } from '../bien-service.service';
import { Data } from './data';
import {KeyWordService} from '../../key-word/key-word.service';

@Component({
  selector: 'app-bien-service-search',
  templateUrl: './bien-service-search.component.html',
  styleUrls: ['./bien-service-search.component.css']
})
export class BienServiceSearchComponent implements OnInit{

  constructor(private service:BienServiceService, private serviceKeyWord:KeyWordService) { }

  private keyWordPropertys : Object[] = [];
  private keyWordServices : Object[] = [];
  private keyWords : Object[] = [];
  private propertys : Object[] = [];
  private services : Object[] = [];
  private noFound : boolean = false;

  private keyWord;

  ngOnInit(): void {
    this.serviceKeyWord.getAllKeyWord().subscribe(res => {
      console.log(res);
      this.keyWordPropertys = res["propertys"];
      this.keyWordServices = res["services"];
    });
  }

  submitKeyWord(value): void {
    this.keyWord = "";
    console.log("mots clef ajouté " + value.keyWord);
    if(!this.keyWords.includes(value.keyWord) && value.keyWord !== undefined){
      this.keyWords.push(value.keyWord);
    }
  }

  removeKeyWord(value): void {
    console.log("mots clef retiré " + value.remove);

    const index = this.keyWords.indexOf(value.remove);
    if (index > -1) {
        this.keyWords.splice(index, 1);
    }
  }

  onSubmit(value): void {
      this.noFound = false;
      this.propertys = [];
      this.services = [];
      if(value.name===undefined){
        value.name="";
      }
      if(value.p===undefined){
        value.p=false;
      }
      if(value.s===undefined){
        value.s=false;
      }
      if(value.am===undefined){
        value.am=false;
      }
      if(value.pm===undefined){
        value.pm=false;
      }
      if(value.name===undefined){
        value.name='';
      }

      var inv=1;
      if(value.inv===true){
        inv=-1;
      }
      console.log(value);
      var trie={};
      if(value.trie === 'price'){
        trie={'price':inv};
      }
      //On vérifie que la dateBegin est supérieur ou égal à la date actuelle, sinon on la change pour ne pas risquer de tomber sur de vieux produit
      const currentDate = new Date();
      const annee   = currentDate.getFullYear();
      const mois    = currentDate.getMonth() + 1;
      const jour    = currentDate.getDate();
      const current = annee + '-' + mois + '-' + jour;
      if(value.dateBegin===undefined || value.dateBegin<current){
        value.dateBegin=current;
      }
      if(value.dateEnd===undefined){
        value.dateEnd=(annee+1000) + '-' + mois + '-' + jour;
      }

      let data = new Data(value.name, value.dateBegin,value.dateEnd, this.keyWords, value.p, value.s, value.am, value.pm, trie);
      console.log(data);
      this.service.getBienService(data).subscribe(res => {
          console.log(res);
          this.propertys = res.propertys;
          this.services = res.services;
          if(this.propertys.length==0 && this.services.length==0){
            this.noFound = true;
          }
      }
      );
  }
}

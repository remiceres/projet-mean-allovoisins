import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BienServiceSearchComponent } from './bien-service-search.component';

describe('BienServiceSearchComponent', () => {
  let component: BienServiceSearchComponent;
  let fixture: ComponentFixture<BienServiceSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BienServiceSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BienServiceSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

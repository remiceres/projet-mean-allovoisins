export class Data {
  constructor(
    public name: string,
    public dateBegin: string,
    public dateEnd: string,
    public keyword: Object[],
    public Propertys: boolean,
    public Services: boolean,
    public am: boolean,
    public pm: boolean,
    public trie: Object,
  ) {}
}

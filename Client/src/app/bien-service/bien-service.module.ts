import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { BienServiceSearchComponent } from './bien-service-search/bien-service-search.component';
import { BienServiceUserComponent } from './bien-service-user/bien-service-user.component';
import { BienServiceAffichageComponent } from './bien-service-affichage/bien-service-affichage.component';
import { BienServiceAddComponent } from './bien-service-add/bien-service-add.component';
import { RouterModule } from '@angular/router';
import {KeyWordModule} from '../key-word/key-word.module';
import {AvailabilityModule} from '../availability/availability.module';
import {ClientModule} from '../client/client.module';
import { BienServiceUseComponent } from './bien-service-use/bien-service-use.component';
import { BeteModule } from '../bete/bete.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    RouterModule,
    KeyWordModule,
    AvailabilityModule,
    ClientModule,
    BeteModule
  ],
  declarations: [BienServiceSearchComponent, BienServiceUserComponent, BienServiceAffichageComponent, BienServiceAddComponent, BienServiceUseComponent],
  exports: [BienServiceSearchComponent, BienServiceUserComponent, BienServiceAffichageComponent, BienServiceAddComponent, BienServiceUseComponent]
})
export class BienServiceModule { }

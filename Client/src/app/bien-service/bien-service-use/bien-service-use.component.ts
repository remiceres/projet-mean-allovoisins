import { Component, OnInit } from '@angular/core';
import { BienServiceService } from '../bien-service.service';
import { AuthServiceService } from '../../auth/auth-service.service';
import {Router} from '@angular/router';
import {DateUsing} from './DateUsing';
@Component({
  selector: 'app-bien-service-use',
  templateUrl: './bien-service-use.component.html',
  styleUrls: ['./bien-service-use.component.css']
})
export class BienServiceUseComponent implements OnInit {

  constructor(private router: Router, private service:BienServiceService, private serviceAuth:AuthServiceService) { }

  private useAM : DateUsing[] = [];
  private usePM : DateUsing[] = [];
  private propertysOld : Object[] = [];
  private servicesOld : Object[] = [];
  private Propertys = 'Propertys';
  private Services = 'Services';
  private week;
  private days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

  ngOnInit(): void {
    this.week = 0;
    this.propertysOld = [];
    this.servicesOld = [];

    this.service.getBienServiceUsing(this.serviceAuth.userConnectedEmail.toString()).subscribe(
        res => {
            console.log(res);
            this.propertysOld = res["propertysOld"];
            this.servicesOld = res["servicesOld"];
            this.getUse();
        },
        err => {
            this.router.navigate( ['/auth' ]);
        }
    );
  }

  getDate(num):string {
    var today= new Date();
    var date=new Date(today.setDate(today.getDate()+num));
    var day=date.getDate();
    var mouth=date.getMonth()+1;
    var year=date.getFullYear();
    return year + '-' + mouth + '-' + day;
  }

  getUse(){
    this.useAM = [];
    this.usePM = [];

    const today= new Date();
    var decalage=today.getDay();
    var k=0;
    if( this.week === 0 ){
      k = today.getDay();
      decalage=0;
    }
    var dateDeb=this.getDate((7*this.week)-decalage);
    var dateFin=this.getDate(6-k+(7*this.week)-decalage);

    this.service.getBienServiceUsingNow(this.serviceAuth.userConnectedEmail.toString(), dateDeb, dateFin).subscribe(
        res => {
            console.log('result : ');
            console.log(res);

            for(let i=0 ; i<7; i++){
                this.useAM.push(new DateUsing(this.getDate(i-k+(7*this.week)-decalage),this.days[i],'',''));
                this.usePM.push(new DateUsing(this.getDate(i-k+(7*this.week)-decalage),this.days[i],'',''));
            }

            for(let i=0 ; i<res.length; i++){
              if(res[i].AMPM==='AM'){
                for(let j=0; j<this.useAM.length; j++){
                  if(this.useAM[j].date === res[i].date){
                    this.useAM[j].id=res[i].idRef;
                    this.useAM[j].type=res[i].propertyOrService;
                  }
                }
              }
              if(res[i].AMPM==='PM'){
                for(let j=0; j<this.usePM.length; j++){
                  if(this.usePM[j].date === res[i].date){
                    this.usePM[j].id=res[i].idRef;
                    this.usePM[j].type=res[i].propertyOrService;
                  }
                }
              }

            }
            console.log(this.useAM);
            console.log(this.usePM);
        }
    );
  }

  beforeWeek(){
    if(this.week !== 0){
      this.week--;
      this.getUse();
    }
  }

  afterWeek(){
    this.week++;
    this.getUse();
  }
}

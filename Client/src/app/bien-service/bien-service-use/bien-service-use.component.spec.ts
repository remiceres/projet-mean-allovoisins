import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BienServiceUseComponent } from './bien-service-use.component';

describe('BienServiceUseComponent', () => {
  let component: BienServiceUseComponent;
  let fixture: ComponentFixture<BienServiceUseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BienServiceUseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BienServiceUseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import {KeyWordService} from '../key-word/key-word.service';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class BienServiceService {

  private pathServeur='http://localhost:8888/';
  constructor(private http:HttpClient) { }

  getBienService(data):Observable<any> {
    const body = JSON.stringify(data);
    var path = this.pathServeur + 'searchProduct/';
    console.log(path, body);
    return this.http.post(path, body, httpOptions);
  }

  getSingleProduct(id, type):Observable<any> {
    var path = this.pathServeur + 'getSingleProducts/' + id + '/' + type;
    console.log(path);
    return this.http.get(path);
  }

  getBienServiceUser(email):Observable<any> {
    var path = this.pathServeur + 'getProducts/' + email;
    return this.http.get(path);
  }

  getBienServiceUsingNow(email, dateDeb, dateEnd):Observable<any> {
    var path = this.pathServeur + 'getProductsUsingNow/' + email + '/' + dateDeb + '/' + dateEnd;
    return this.http.get(path);
  }

  getBienServiceUsing(email):Observable<any> {
    var path = this.pathServeur + 'getProductsUsing/' + email;
    return this.http.get(path);
  }

  addProducts(product, type):Observable<any> {
      const body = JSON.stringify(product);
      return this.http.post(this.pathServeur + 'addProducts/' + type, body, httpOptions);
  }

  remove(id, type):Observable<any> {
    var path = this.pathServeur + 'removeProduct/' + id + '/' + type;
    return this.http.get(path);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BienServiceAffichageComponent } from './bien-service-affichage.component';

describe('BienServiceAffichageComponent', () => {
  let component: BienServiceAffichageComponent;
  let fixture: ComponentFixture<BienServiceAffichageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BienServiceAffichageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BienServiceAffichageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

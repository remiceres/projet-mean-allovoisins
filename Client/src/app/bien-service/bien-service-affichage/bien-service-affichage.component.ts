import { Component, OnInit } from '@angular/core';
import { BienServiceService } from '../bien-service.service';
import {ActivatedRoute} from '@angular/router'
import {Router} from '@angular/router';
import { AuthServiceService } from '../../auth/auth-service.service';
import { ClientService } from '../../client/client.service';

@Component({
  selector: 'app-bien-service-affichage',
  templateUrl: './bien-service-affichage.component.html',
  styleUrls: ['./bien-service-affichage.component.css']
})
export class BienServiceAffichageComponent implements OnInit {

  private id = '';
  private type = '';
  private owner = '';
  private products : Object[];
  private money : string;


  constructor(private router: Router, private service:BienServiceService, private route: ActivatedRoute, private serviceAuth:AuthServiceService, private serviceClient:ClientService) {}


  ngOnInit() {
      if (this.type === "Propertys")
      {
          this.money = "€"
      }
      else
      {
          this.money = "€/h"
      }
    this.id=this.route.snapshot.params['id'];
    this.type=this.route.snapshot.params['type'];
    this.service.getSingleProduct(this.id, this.type).subscribe(res => {
      console.log(res);
      this.products=res;
      this.owner= res[0].emailMembre;
    });
  }

  remove(): void {
    console.log('remove : ' + this.id + ', type : ' + this.type);
    this.service.remove(this.id, this.type).subscribe(res => {});
    this.router.navigate( ['bien-service-user']);
  }
}

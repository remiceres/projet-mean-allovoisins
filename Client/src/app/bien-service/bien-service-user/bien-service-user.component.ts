import { Component, OnInit } from '@angular/core';
import { BienServiceService } from '../bien-service.service';
import { AuthServiceService } from '../../auth/auth-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-bien-service-user',
  templateUrl: './bien-service-user.component.html',
  styleUrls: ['./bien-service-user.component.css']
})
export class BienServiceUserComponent implements OnInit{

  constructor(private router: Router, private service:BienServiceService, private serviceAuth:AuthServiceService) { }

  private propertysAvailable : Object[] = [];
  private servicesAvailable : Object[] = [];
  private propertysUnavailable : Object[] = [];
  private servicesUnavailable : Object[] = [];
  private Propertys = 'Propertys';
  private Services = 'Services';

  ngOnInit(): void {
    this.propertysAvailable = [];
    this.servicesAvailable = [];
    this.propertysUnavailable = [];
    this.servicesUnavailable = [];
    this.service.getBienServiceUser(this.serviceAuth.userConnectedEmail.toString()).subscribe(
        res => {
            console.log(res);
            this.propertysAvailable = res["propertysAvailable"];
            this.servicesAvailable = res["servicesAvailable"];
            this.propertysUnavailable = res["propertysUnavailable"];
            this.servicesUnavailable = res["servicesUnavailable"];
        },
        err => {
            this.router.navigate( ['/auth' ]);
        }
    );
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BienServiceUserComponent } from './bien-service-user.component';

describe('BienServiceUserComponent', () => {
  let component: BienServiceUserComponent;
  let fixture: ComponentFixture<BienServiceUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BienServiceUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BienServiceUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

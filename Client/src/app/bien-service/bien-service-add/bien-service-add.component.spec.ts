import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BienServiceAddComponent } from './bien-service-add.component';

describe('BienServiceAddComponent', () => {
  let component: BienServiceAddComponent;
  let fixture: ComponentFixture<BienServiceAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BienServiceAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BienServiceAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

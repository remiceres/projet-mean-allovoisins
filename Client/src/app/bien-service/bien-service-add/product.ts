export class Product {
  constructor(
    public emailMembre: string,
    public name: string,
    public desc: string,
    public price: number,
    public picture: string
  ) {}
}

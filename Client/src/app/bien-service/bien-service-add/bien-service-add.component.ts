import { Component, OnInit} from '@angular/core';
import { BienServiceService } from '../bien-service.service';
import {Router} from '@angular/router';
import { Product } from './product';
import { AuthServiceService } from '../../auth/auth-service.service';

@Component({
  selector: 'app-bien-service-add',
  templateUrl: './bien-service-add.component.html',
  styleUrls: ['./bien-service-add.component.css']
})
export class BienServiceAddComponent implements OnInit {

  private errorMessage;

  constructor(private service:BienServiceService, private router: Router, private serviceAuth:AuthServiceService) { }

  ngOnInit(): void {
    if(this.serviceAuth.userConnectedEmail === ''){
      this.router.navigate( ['/auth' ]);
    }
  }

  onSubmit(value): void {
      console.log(value.PropertysServices)
      if(value.PropertysServices===undefined){
        value.PropertysServices='Propertys'
      }

      this.errorMessage = ""
      if (value.desc === undefined) {
          value.desc = '';
      }
      let product = new Product(this.serviceAuth.userConnectedEmail.toString(), value.name, value.desc, value.price, value.picture);
      this.service.addProducts(product, value.PropertysServices).subscribe(
           res =>{
               console.log("succes : " + res.id + " de type : " + value.PropertysServices);
               this.router.navigate( ['bien-service-print/', res.id, value.PropertysServices] );
          },
          err => {
              console.log(err);
              this.errorMessage = err.error.message
          }

      )
  }

}

import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private pathServeur='http://localhost:8888/';
  private ratioLimit = "-30";
  public limitUser=false;

  constructor(private http:HttpClient) { }

  userRatioLimit():Observable<any> {
    return this.http.get(this.pathServeur + 'userRatioLimit/' + this.ratioLimit);
  }
}

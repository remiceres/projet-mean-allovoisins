import { Component, OnInit } from '@angular/core';
import {AdminService} from '../admin.service';

@Component({
  selector: 'app-admin-ratio',
  templateUrl: './admin-ratio.component.html',
  styleUrls: ['./admin-ratio.component.css']
})
export class AdminRatioComponent implements OnInit {

  private memberLimits : Object[];

  constructor(private service:AdminService) { }

  ngOnInit() {
    this.service.userRatioLimit().subscribe(res => {
      console.log(res);
      this.memberLimits=res;
      console.log("res : ");
      console.log(res);
    });
  }

}

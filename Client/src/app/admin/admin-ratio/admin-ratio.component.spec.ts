import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRatioComponent } from './admin-ratio.component';

describe('AdminRatioComponent', () => {
  let component: AdminRatioComponent;
  let fixture: ComponentFixture<AdminRatioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRatioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRatioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRatioComponent } from './admin-ratio/admin-ratio.component';

import {ClientModule} from '../client/client.module';
import { BeteModule } from '../bete/bete.module';

@NgModule({
  declarations: [AdminRatioComponent],
  imports: [
    CommonModule,
    ClientModule,
    BeteModule
  ],
  exports: [AdminRatioComponent]
})
export class AdminModule { }

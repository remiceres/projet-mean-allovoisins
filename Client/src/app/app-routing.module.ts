import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {BienServiceSearchComponent} from './bien-service/bien-service-search/bien-service-search.component';
import {UserListComponent} from './client/user-list/user-list.component';
import {RegisterComponent} from './client/register/register.component';
import {UserSearchComponent } from './client/user-search/user-search.component';
import {BienServiceUserComponent} from './bien-service/bien-service-user/bien-service-user.component';
import {BienServiceUseComponent} from './bien-service/bien-service-use/bien-service-use.component';
import { BienServiceAffichageComponent } from './bien-service/bien-service-affichage/bien-service-affichage.component';
import { BienServiceAddComponent } from './bien-service/bien-service-add/bien-service-add.component';
import { AuthUserComponent } from './auth/auth-user/auth-user.component';
import {AdminRatioComponent} from './admin/admin-ratio/admin-ratio.component';
import {UserAffichageComponent} from './client/user-affichage/user-affichage.component';
const routes: Routes = [
  {
  path: '', component : BienServiceSearchComponent
  },
  // {
  // path: 'user-list', component: UserListComponent, outlet: 'memberPlace'
  // },
  {
  path: 'register', component: RegisterComponent
  },
  {
  path: 'user-search', component: UserSearchComponent
  },
  {
  path: 'bien-service-user', component: BienServiceUserComponent
  },
  {
  path: 'bien-service-using', component: BienServiceUseComponent
  },
  {
  path: 'bien-service-print/:id/:type', component: BienServiceAffichageComponent
  },
  {
  path: 'bien-service-add', component: BienServiceAddComponent
  },
  {
  path: 'auth', component: AuthUserComponent
  },
  {
  path: 'admin', component: AdminRatioComponent
  },
  {
  path: 'user-print/:email', component: UserAffichageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

Créé un module dans un autre
```
ng g m client -m=app.module
```
---
Créé un composant dans un module et exporté
```
ng g c client/user-list --export=true
```
---
Créé un services
```
ng g s client/client
```
---
Option pour voir ce qui ce passe sans l'exécuté
```
-d
```
---
importé HTTPClient dans APP pour permetre l'injection des dépandances.
---
ajout de l'entete des CORS dans le serveurs.
---

#!/bin/bash

# import files
for file in ${dir}*.json ; do
    colection=$(echo $file| cut -d'.' -f 1)
    echo "Imort : $file"
    mongoimport --db TROC --collection $colection --file $file --jsonArray --drop
done

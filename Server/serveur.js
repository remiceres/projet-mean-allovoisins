'use strict';

// assert import
const assert = require('assert');
const morgan = require('morgan');

// server import
const express = require('express');
const app = express();


app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(morgan('tiny'));


// mongoDB import
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';



// connection to mongoDB
let db;
MongoClient.connect(url, {useNewUrlParser: true}, (err, client) => {
    db = client.db('TROC');
    assert.equal(null, err);
});

// configuration du serveur
app.listen(8888);
console.log('address: http://localhost:8888');



///////////////////////
////  web services ////
///////////////////////

// // calcule du nouveau ID
// const availability = await db.collection('Users')
//     .find()
//     .toArray();
//
// const id = Math.max.apply(Math, availability.map(x => x.idRef)) + 1;

// inscription
//////////////
app.post('/addUser', async (req, res) => {
    // configuration de la réponse
    res.setHeader('Content-type', 'text/plain');
    // test de la présence des paramètre requis
    if (req.body.last_name === ''
        || req.body.first_name === ''
        || req.body.email === ''
        || req.body.password === ''
        || req.body.address === ''
        || req.body.City === ''
        || req.body.Phone === ''
    ) {
        res.status(400);
        res.end(JSON.stringify({'message': 'user not have all require attributs'}));
        return;
    }

    // test de l'unisité de l'email
    const availability = await db.collection('Users')
        .find({'email': req.body.email})
        .toArray();

    if (availability.length !== 0) {
        res.status(409);
        res.end(JSON.stringify({'message': 'email alredy use'}));
        return;
    }

    // ajout de l'utilisateur
    await db.collection('Users').insertOne({'last_name':req.body.last_name, 'first_name':req.body.first_name, 'email':req.body.email, 'password':req.body.password, 'address':req.body.address, 'city':req.body.city, 'phone':req.body.phone, 'birthday':req.body.birthday, 'role':req.body.role, 'register_date':req.body.register_date, 'avatar':req.body.avatar, 'sell':parseInt(req.body.sell, 10), 'buy':parseInt(req.body.buy, 10), 'ratio':parseInt(req.body.ratio, 10)}, (err) => {
        if (err) {
            res.status(400);
            res.end(JSON.stringify({'message': 'Bad Request'}));
        } else {
            res.status(201);
            res.end(JSON.stringify({'message': 'Created'}));
        }
    });
});

//Changer image, num, adresse ou ville d'un user
app.post('/changeParamsUser/:email', (req, res) => {
    // configuration de la réponse
    res.setHeader('Content-type', 'text/plain');
    // test de la présence des paramètre requis
    db.collection('Users')
        .updateOne({'email':req.params.email}, {$set:req.body});
});

// ajout d'un produit
//////////////
app.post('/addProducts/:type', async (req, res) => {
    // configuration de la réponse
    res.setHeader('Content-type', 'text/plain');
    // test de la présence des paramètre requis
    if (req.body.type === ''
        || req.body.emailMembre === ''
        || req.body.name === ''
        || req.body.price === ''
        || req.body.picture === ''
    ) {
        res.status(400);
        res.end(JSON.stringify({'message': 'product not have all require attributs'}));
        return;
    }

    if (req.body.price < 0) {
        res.status(400);
        res.end(JSON.stringify({'message': 'Le prix est négatif !'}));
        return;
    }

    if (req.body.desc === undefined) {
        req.body.desc = '';
    }

    // trouve l'id max des produit actuel
    const products = await db.collection(req.params.type)
        .find()
        .toArray();
    const id = products.map(x => x.id);
    req.body.id = '' + (Math.max.apply(Math, id) + 1);
    req.body.price = parseInt(req.body.price, 10);

    const result = JSON.stringify(req.body);

    console.log(result);

    // ajout de l'utilisateur
    await db.collection(req.params.type).insertOne(req.body, (err) => {
        if (err) {
            res.status(400);
            res.end(JSON.stringify({'message': 'Bad Request'}));
        } else {
            console.log('result\n' + result);
            res.end(result);
        }
    });

});

//Recherche biens et service d'un utilisateur
app.get('/getProducts/:email', async (req, res) => {
    console.log('je recherche les produit de : ' + req.params.email);
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');

    const result = {};
    result['propertysAvailable'] = [];
    result['servicesAvailable'] = [];
    result['propertysUnavailable'] = [];
    result['servicesUnavailable'] = [];

    const currentDate = new Date();
    const annee   = currentDate.getFullYear();
    const mois    = currentDate.getMonth() + 1;
    const jour    = currentDate.getDate();
    const current = annee + '-' + mois + '-' + jour;

    //On récupère les id des bien et service disponible
    const propertyAvailable = await db.collection('Availability')
        .find({'propertyOrService':'Propertys', 'date':{$gte:current}})
        .toArray();
    const serviceAvailable = await db.collection('Availability')
        .find({'propertyOrService':'Services', 'date':{$gte:current}})
        .toArray();
    var idProperty = propertyAvailable.map(x => x.idRef);
    var idService = serviceAvailable.map(x => x.idRef);
    //On récupère id des service pris
    var propertyUse = await db.collection('Utilization')
        .find({'propertyOrService':'Propertys', 'date':{$gte:current}})
        .toArray();
    const serviceUse = await db.collection('Utilization')
        .find({'propertyOrService':'Services', 'date':{$gte:current}})
        .toArray();
    const idPropertyUse = propertyUse.map(x => x.idRef);
    const idServiceUse = serviceUse.map(x => x.idRef);

    idProperty = idProperty.concat(idPropertyUse);
    idService = idService.concat(idServiceUse);

    //On remplis notre resultat avec les produit du client Disponible
    result['propertysAvailable'] = await db.collection('Propertys')
        .find({'emailMembre': req.params.email, 'id':{$in: idProperty}})
        .toArray();
    result['servicesAvailable'] = await db.collection('Services')
        .find({'emailMembre': req.params.email, 'id':{$in: idService}})
        .toArray();

    //On remplis notre resultat avec les produit du client Indisponible
    result['propertysUnavailable'] = await db.collection('Propertys')
        .find({'emailMembre': req.params.email, 'id':{$nin: idProperty}})
        .toArray();
    result['servicesUnavailable'] = await db.collection('Services')
        .find({'emailMembre': req.params.email, 'id':{$nin: idService}})
        .toArray();

    console.log(JSON.stringify(result));
    res.end(JSON.stringify(result));
});

app.get('/getProductsUsingNow/:email/:dateDeb/:dateEnd', async (req, res) => {
    console.log('je recherche les produit de : ' + req.params.email);
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');

    var result = [];

    //On récupère les produit pris par le user
    result = await db.collection('Utilization')
        .find({'mailUser':req.params.email, 'date':{$gte:req.params.dateDeb, $lte:req.params.dateEnd}})
        .toArray();

    console.log(JSON.stringify(result));
    res.end(JSON.stringify(result));
});

//recherche vieux produit utilisé par un user
app.get('/getProductsUsing/:email', async (req, res) => {
    console.log('je recherche les vieux produit de : ' + req.params.email);
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');

    const result = {};

    result['propertysOld'] = [];
    result['servicesOld'] = [];

    const currentDate = new Date();
    const annee   = currentDate.getFullYear();
    const mois    = currentDate.getMonth() + 1;
    const jour    = currentDate.getDate();
    const current = annee + '-' + mois + '-' + jour;

    //On récupère les id des produit pris pas le user
    var propertyOld = await db.collection('Utilization')
        .find({'propertyOrService':'Propertys', 'mailUser':req.params.email, 'date':{$lt:current}})
        .toArray();
    const serviceOld = await db.collection('Utilization')
        .find({'propertyOrService':'Services', 'mailUser':req.params.email, 'date':{$lt:current}})
        .toArray();
    const idPropertyOld = propertyOld.map(x => x.idRef);
    const idServiceOld  = serviceOld.map(x => x.idRef);

    console.log('propertyOld' + JSON.stringify(idPropertyOld));
    console.log('serviceOld' + JSON.stringify(idServiceOld));

    //On récupère les vieux produit pris par le user
    result['propertysOld'] = await db.collection('Propertys')
        .find({'id':{$in:idPropertyOld}})
        .toArray();
    result['servicesOld'] = await db.collection('Services')
        .find({'id':{$in:idServiceOld}})
        .toArray();


    console.log(JSON.stringify(result));
    res.end(JSON.stringify(result));
});

//Recherche un produit
app.get('/getSingleProducts/:id/:type', async (req, res) => {
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');
    console.log('\ngetSingleProduct : ' + req.params.id + '/' + req.params.type);

    const result = await db.collection(req.params.type)
        .find({'id':req.params.id})
        .toArray();
    console.log('resultat : ' + JSON.stringify(result));
    res.end(JSON.stringify(result));
});

//Recherche disponibilité d'un produit
app.get('/availability/:id/:type/:dateDeb/:dateFin', async (req, res) => {
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');

    console.log('demande des disponibilité');

    var result = {};
    result['utilization'] = [];
    result['availability'] = [];
    //On récupère les date ou elle sont utilisées
    const utilization = await db.collection('Utilization')
        .find({'propertyOrService':req.params.type, 'idRef':req.params.id, 'date':{$gte:req.params.dateDeb, $lte:req.params.dateFin}})
        .toArray();

    //On récupère les date ou elle sont disponible
    const availability = await db.collection('Availability')
        .find({'propertyOrService':req.params.type, 'idRef':req.params.id, 'date':{$gte:req.params.dateDeb, $lte:req.params.dateFin}})
        .toArray();

    //On reformate les date
    console.log(JSON.stringify(utilization));
    console.log(JSON.stringify(availability));

    for (let i = 0; i < utilization.length; i++) {
        result['utilization'].push(utilization[i].date + '/' + utilization[i].AMPM);
    }
    for (let j = 0; j < availability.length; j++) {
        result['availability'].push(availability[j].date + '/' + availability[j].AMPM);
    }

    console.log(JSON.stringify(result));

    res.end(JSON.stringify(result));
});

//On cherche l'utilisateur qui utilise un produit
app.get('/getUserUtilization/:id/:type/:date/:AMPM', async (req, res) => {
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');

    const result = await db.collection('Utilization')
        .find({'idRef':req.params.id, 'propertyOrService':req.params.type, 'date':req.params.date, 'AMPM':req.params.AMPM})
        .toArray();
    console.log('resultat : ' + JSON.stringify(result));
    res.end(JSON.stringify(result));
});

//On cherche les produit que utilise l'utilisateur a une date préssise
app.get('/verifUse/:date/:AMPM/:email', async (req, res) => {
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');

    const result = await db.collection('Utilization')
        .find({'date':req.params.date, 'AMPM':req.params.AMPM, 'mailUser':req.params.email})
        .toArray();
    console.log('resultat : ' + JSON.stringify(result));
    res.end(JSON.stringify(result));
});

//Ajouter une disponibilité à un produit
app.get('/addAvaibility/:id/:type/:date/:AMPM', (req, res) => {
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');
    console.log('addAvaibility' + JSON.stringify(req.params));

    db.collection('Availability').insertOne(
        {'propertyOrService':req.params.type, 'idRef':req.params.id, 'date':req.params.date, 'AMPM':req.params.AMPM}
        , (err) => {
            if (err) {
                res.status(400);
                res.end(JSON.stringify({'message': 'Bad Request'}));
            } else {
                res.end(JSON.stringify({'message': 'Sucess'}));
            }
        });
});

//retirer une disponibilité à un produit
app.get('/removeAvaibility/:id/:type/:date/:AMPM', (req, res) => {
    console.log('removeAvaibility' + JSON.stringify(req.params));

    db.collection('Availability')
        .deleteMany({'propertyOrService':req.params.type, 'idRef':req.params.id, 'date':req.params.date, 'AMPM':req.params.AMPM}, function (err, obj) {
            console.log(' products deleted' + obj);
            res.end(JSON.stringify({'message': 'Succes'}));
        });
});

//Ajouter une utilisation de plusieurs produits
app.post('/addUtilization', async (req, res) => {
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');
    console.log('addUtilization' + JSON.stringify(req.body));

    for (let i = 0; i < req.body.length; i++) {
        await db.collection('Utilization').insertOne(
            {'propertyOrService':req.body[i].type, 'mailUser':req.body[i].user, 'idRef':req.body[i].id, 'date':req.body[i].date, 'AMPM':req.body[i].AMPM}
            , (err) => {
                if (err) {
                    res.status(400);
                    res.end(JSON.stringify({'message': 'Bad Request'}));
                } else {
                    res.end(JSON.stringify({'message': 'Sucess'}));
                }
            });
        //Retirer la disponibilité
        console.log('*** removeAvaibility' + JSON.stringify(req.body[i]));

        await db.collection('Availability')
            .deleteMany({'propertyOrService':req.body[i].type, 'idRef':req.body[i].id, 'date':req.body[i].date, 'AMPM':req.body[i].AMPM}, function (err, obj) {
                console.log(' products deleted' + obj);
                res.end(JSON.stringify({'message': 'succes'}));
            });
    }

    //Calcul des ratios
    const ownerP = await db.collection(req.body[0].type)
        .find({'id':req.body[0].id})
        .toArray();
    console.log(ownerP);
    const owner = await db.collection('Users')
        .find({'email':ownerP[0].emailMembre})
        .toArray();
    console.log(owner);
    const sell = owner[0].sell + req.body.length;
    const ownerRatio = sell - owner[0].buy;
    const user = await db.collection('Users')
        .find({'email':req.body[0].user})
        .toArray();
    console.log(user);
    const buy = user[0].buy + req.body.length;
    const userRatio = user[0].sell - buy;
    await db.collection('Users')
        .updateOne({'email':owner[0].email}, {$set:{'sell':sell, 'ratio':ownerRatio}});
    await db.collection('Users')
        .updateOne({'email':user[0].email}, {$set:{'buy':buy, 'ratio':userRatio}});

    console.log('owner' + owner[0].sell + ':' + owner[0].buy + ', user ' + user[0].sell + ':' + user[0].buy);
});

//retirer une utilisation à un produit
app.get('/removeUtilization/:id/:type/:date/:AMPM/:user', async (req, res) => {
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');
    console.log('removeUtilization' + JSON.stringify(req.params));

    await db.collection('Utilization')
        .deleteMany({'propertyOrService':req.params.type, 'mailUser':req.params.user, 'idRef':req.params.id, 'date':req.params.date, 'AMPM':req.params.AMPM}, function (err, obj) {
            console.log(' products deleted' + obj);
            res.end(JSON.stringify({'message': 'succes'}));
        });

    //ajouter la disponibilité
    console.log('***addAvailability' + JSON.stringify(req.params));

    await db.collection('Availability').insertOne(
        {'propertyOrService':req.params.type, 'idRef':req.params.id, 'date':req.params.date, 'AMPM':req.params.AMPM}
        , (err) => {
            if (err) {
                res.status(400);
                res.end(JSON.stringify({'message': 'Bad Request'}));
            } else {
                res.end(JSON.stringify({'message': 'Sucess'}));
            }
        });

    //Calcul des ratios
    const ownerP = await db.collection(req.params.type)
        .find({'id':req.params.id})
        .toArray();

    const owner = await db.collection('Users')
        .find({'email':ownerP[0].emailMembre})
        .toArray();
    const sell = owner[0].sell - 1;
    const ownerRatio = sell - owner[0].buy;
    const user = await db.collection('Users')
        .find({'email':req.params.user})
        .toArray();
    const buy = user[0].buy - 1;
    const userRatio = user[0].sell - buy;
    await db.collection('Users')
        .updateOne({'email':owner[0].email}, {$set:{'sell':sell, 'ratio':ownerRatio}});
    await db.collection('Users')
        .updateOne({'email':user[0].email}, {$set:{'buy':buy, 'ratio':userRatio}});
});

//On remove un produit
app.get('/removeProduct/:id/:type', async (req, res) => {
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');

    //On supprime le produit
    await db.collection(req.params.type)
        .deleteMany({'id':req.params.id}, function (err, obj) {
            console.log(' products deleted' + obj);
            res.end(JSON.stringify({'message': 'succes'}));
        });

    //On supprime les disponibilité du produit
    var propertyOrService = 'property';
    if (req.params.type === 'Services') {
        propertyOrService = 'service';
    }
    await db.collection('Availability')
        .deleteMany({'idRef':req.params.id, 'propertyOrService':propertyOrService}, function (err, obj) {
            console.log(err);
            console.log(' availability deleted' + obj);
        });
    await db.collection('Utilization')
        .deleteMany({'idRef':req.params.id, 'propertyOrService':propertyOrService}, function (err, obj) {
            console.log(err);
            console.log(' utilisation deleted' + obj);
        });

    await db.collection('Descriptive' + req.params.type)
        .deleteMany({'id':req.params.id}, function (err, obj) {
            console.log(err);
            console.log(' keyWord deleted' + obj);
        });
    res.end(JSON.stringify({'message': 'succes'}));
});

// Recherche biens et services multicritère (Utilise un object pour les critère)
/////////////////////////
//Critère : name : obligatoire -> le nom ou une partie du nom de ce qu'on charche
//          date : plusieur et obligatoire -> récupère strictement ceux qui sont à cette date
//          keyword : plusieur et pas obligatoire -> récupère strictement ceux qui on ces mots clef
//          Propertys : booléen si on veux des biens
//          Services : booléen si on veux des services
app.post('/searchProduct', async (req, res) => {
    console.log('\n**On recherche : ' + JSON.stringify(req.body) + '**\n');
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');
    var regex = '.*' + req.body.name + '.*';

    // initialisation
    const result = {};
    result['propertys'] = [];
    result['services'] = [];

    //On vérifie si on veux le produit le matin ou après midi
    var regexAMPM = '';
    if (req.body.am) {
        regexAMPM = 'AM';
    }
    if (req.body.am && req.body.pm) {
        regexAMPM = regexAMPM + '|';
    }
    if (req.body.pm) {
        regexAMPM = regexAMPM + 'PM';
    }
    console.log(regexAMPM);

    //Bien
    if (req.body.Propertys) {
        //On récupère les biens disponible à la date indiqué
        const availability = await db.collection('Availability')
            .find({'date':{$gte:req.body.dateBegin, $lte:req.body.dateEnd}, 'AMPM':new RegExp(regexAMPM, 'i'), 'propertyOrService' : 'Propertys'})
            .toArray();
        //On les regroupe par id
        const availId = availability.map(x => x.idRef);
        console.log('availability property : ' + JSON.stringify(availId) + '\n');

        let keywordId = availId;
        if (req.body.keyword.length > 0) {
            //On récupère dans l'id des biens qui corresponde
            //a un mot clef indiqué
            const propKeyword = await db.collection('DescriptivePropertys')
                .find({'keyword':{$in: req.body.keyword}, 'id':{$in:  availId}})
                .toArray();
            keywordId = propKeyword.map(x => x.id);

            console.log('property avec motClef : ' + JSON.stringify(keywordId) + '\n');
        }

        // recupere les property qui possède dans leur nom le mots de recherche principal indiqué
        const propertys = await db.collection('Propertys')
            .find({'id':{$in: keywordId}, 'name':new RegExp(regex, 'i')})
            .sort(req.body.trie)
            .toArray();

        result['propertys'] = propertys;
        console.log(propertys);
    }

    // services
    if (req.body.Services) {
        //On récupère les services disponible à la date indiqué
        const availability = await db.collection('Availability')
            .find({'date':{$gte:req.body.dateBegin, $lte:req.body.dateEnd}, 'AMPM':new RegExp(regexAMPM, 'i'), 'propertyOrService' : 'Services'})
            .toArray();

        //On les regroupe par id
        const availId = availability.map(x => x.idRef);
        console.log('availability services: ' + JSON.stringify(availId) + '\n');

        let keywordId = availId;
        if (req.body.keyword.length > 0) {
            //On récupère dans l'id des services qui corresponde
            //a un mot clef indiqué
            const servKeyword = await db.collection('DescriptiveServices')
                .find({'keyword':{$in: req.body.keyword}, 'id':{$in: availId}})
                .toArray();
            keywordId = servKeyword.map(x => x.id);
            console.log('service avec motClef : ' + JSON.stringify(keywordId) + '\n');
        }

        // recupère les services qui possède dans leur nom le mots de recherche principal indiqué
        const services = await db.collection('Services')
            .find({'id':{$in: keywordId}, 'name':new RegExp(regex, 'i')})
            .sort(req.body.trie)
            .toArray();

        result['services'] = services;
        console.log(services);
    }
    res.end(JSON.stringify(result));
});

app.get('/getKeyWord', async (req, res) => {
    const result = {};
    console.log('getAllKeyWord');
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');

    //On récupère les mots clef dans DescriptivePropertys
    const keyProperty = await db.collection('DescriptivePropertys')
        .find()
        .toArray();

    //On récupère les mots clef dans DescriptivePropertys
    const keyService = await db.collection('DescriptiveServices')
        .find()
        .toArray();

    //On récupère les mots clef sans doublons
    result['propertys'] = Array.from(new Set(keyProperty.map(x => x.keyword)));
    result['services'] = Array.from(new Set(keyService.map(x => x.keyword)));

    res.end(JSON.stringify(result));
});

app.get('/getKeyWord/:id/:type', async (req, res) => {
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');
    console.log(req.params.id  + ' et ' + req.params.type);

    //On récupère les mots clefs
    const result = await db.collection('Descriptive' + req.params.type)
        .find({'id':req.params.id})
        .toArray();
    console.log('return :' + JSON.stringify(result));
    res.end(JSON.stringify(result));
});

app.get('/removeKeyWord/:id/:type/:keyword', async (req, res) => {
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');
    console.log(req.params.id  + ' et ' + req.params.type);

    //On supprime le mot clef
    await db.collection('Descriptive' + req.params.type)
        .deleteMany({'id':req.params.id, 'keyword':req.params.keyword}, function (err, obj) {
            console.log(' keyWord deleted' + obj);
            res.end(JSON.stringify({'message': 'succes'}));
        });
});

//ajout d'un keyWord
app.post('/addKeyWord/:type', async (req, res) => {
    // configuration de la réponse
    res.setHeader('Content-type', 'text/plain');
    // test de la présence des paramètre requis
    if (req.body.keyword === ''
    ) {
        res.status(400);
        res.end(JSON.stringify({'message': 'le mot clef ne peut pas être vide.'}));
        return;
    }

    //On recherche si le keyword n'existe pas déjà
    const sameKeyWord = await db.collection('Descriptive' + req.params.type)
        .find({'id':req.body.id, 'keyword':req.body.keyword})
        .toArray();
    if (sameKeyWord.length > 0) {
        res.status(400);
        res.end(JSON.stringify({'message': 'le mot clef existe déjà pour ce produit.'}));
        return;
    }

    // ajout du mot clef
    await db.collection('Descriptive' + req.params.type).insertOne(req.body, (err) => {
        if (err) {
            res.status(400);
            res.end(JSON.stringify({'message': 'Bad Request'}));
        }
    });

});

// get user
///////////

app.get('/getUsers', async (req, res) => {
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');

    //On récupère les biens/services dans disponibilité grace à une date
    const result = await db.collection('Users')
        .find()
        .toArray();

    res.end(JSON.stringify(result));
});

// get user by name
///////////////////
app.post('/getUsersByName/', async (req, res) => {
    console.log('je recherche : ' + req.params.name + ' -> ' + req.body);
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');

    var regex = '.*' + req.body.name + '.*'; //Recherche ce qui contien name en sous chaine, l'option i se moque des majuscule/minuscule

    const result = await db.collection('Users')
        .find({$or: [{'last_name': new RegExp(regex, 'i')},
            {'first_name': new RegExp(regex, 'i')}]})
        .sort(req.body.trie)
        .toArray();

    console.log(JSON.stringify(result));
    res.end(JSON.stringify(result));
});

//Supprime le compte d'un utilisateur
app.get('/removeUsersByID/:id', async (req, res) => {

    const Objpropertys = await db.collection('Propertys')
        .find({'emailMembre': req.params.id})
        .toArray();
    const propertys = Objpropertys.map(x => x.id);

    const Objservices = await db.collection('Services')
        .find({'emailMembre': req.params.id})
        .toArray();
    const services = Objservices.map(x => x.id);

    await db.collection('Users')
        .deleteMany({'email': req.params.id}, function (err, obj) {
            console.log(err);
            console.log('User' + obj);
        });

    db.collection('Utilization')
        .deleteMany({'mailUser': req.params.id}, function (err, obj) {
            console.log(err);
            console.log('Use' + obj);
        });

    db.collection('DescriptivePropertys')
        .deleteMany({'id': {$in:propertys}}, function (err, obj) {
            console.log(err);
            console.log('DescProperty' + obj);
        });
    db.collection('DescriptiveServices')
        .deleteMany({'id': {$in:services}}, function (err, obj) {
            console.log(err);
            console.log('Descservice' + obj);
        });

    db.collection('Availability')
        .deleteMany({'propertyOrService':'Propertys', 'idRef': {$in:propertys}}, function (err, obj) {
            console.log(err);
            console.log('AvaiProperty' + obj);
        });
    db.collection('Availability')
        .deleteMany({'propertyOrService':'Services', 'idRef': {$in:services}}, function (err, obj) {
            console.log(err);
            console.log('Avaiservice' + obj);
        });

    db.collection('Utilization')
        .deleteMany({'propertyOrService':'Propertys', 'idRef': {$in:propertys}}, function (err, obj) {
            console.log(err);
            console.log('Useservice' + obj);
        });
    db.collection('Utilization')
        .deleteMany({'propertyOrService':'Services', 'idRef': {$in:services}}, function (err, obj) {
            console.log(err);
            console.log('Useservice' + obj);
        });

    await db.collection('Propertys')
        .deleteMany({'emailMembre': req.params.id}, function (err, obj) {
            console.log(err);
            console.log('property' + obj);
        });
    await db.collection('Services')
        .deleteMany({'emailMembre': req.params.id}, function (err, obj) {
            console.log(err);
            console.log('service' + obj);
        });

    console.log('delete user sucess');
    res.end(JSON.stringify({'message' : 'succes'}));


});
// get user by id
///////////////////
app.get('/getUsersByID/:id', async (req, res) => {
    // console.log('je recherche l utilisateur d id : ' + req.params.id);
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');

    const result = await db.collection('Users')
        .find({'email': req.params.id})
        .toArray();

    // console.log(JSON.stringify(result));
    res.end(JSON.stringify(result));
});

//Vérifie l'authentification d'un user
app.get('/authentification/:email/:password', async (req, res) => {
    console.log('authentification de : ' + req.params.email);
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');

    //On vérifie dans un premier temps que cette adresse mail existe
    const present = await db.collection('Users')
        .find({'email':req.params.email})
        .toArray();
    if (present.length === 0) {
        res.status(400);
        res.end(JSON.stringify({'message': 'adresse mail inconnue'}));
        return;
    }

    console.log('adresse présente');

    const result = await db.collection('Users')
        .find({'email':req.params.email, 'password':req.params.password})
        .toArray();

    console.log(JSON.stringify(result));
    if (result.length === 0) {
        res.status(400);
        res.end(JSON.stringify({'message': 'Erreur mauvais mot de passe !'}));
        return;
    }
    if (result.length > 1) {
        res.status(400);
        res.end(JSON.stringify({'message': 'Erreur inatendu, plusieurs comptes trouvés !'}));
        return;
    }

    console.log('connexion réussite : ' + JSON.stringify(result));

    res.end(JSON.stringify(result));
});

//Recupère les user qui on buy+limit>sell
app.get('/userRatioLimit/:limit', async (req, res) => {
    // console.log('je recherche l utilisateur d id : ' + req.params.id);
    // configuration de la réponse
    res.setHeader('Content-type', 'application/json');
    console.log(req.params.limit);
    console.log(parseInt(req.params.limit, 10));
    const result = await db.collection('Users')
        .find({'ratio':{$lt:parseInt(req.params.limit, 10)}})
        .toArray();

    // console.log(JSON.stringify(result));
    res.end(JSON.stringify(result));
});
//curl --header 'content-type: application/json' -X POST --data '{"name":"para", "date":{"numWeek":"3"}, "keyword":["ecole"], "Propertys":true, "Services":true}' localhost:8888/Availability

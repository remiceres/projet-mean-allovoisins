'use strict';
var assert = require('assert');
var express = require('express');
var MongoClient = require('mongodb').MongoClient;

var app = express();
var url = 'mongodb://localhost:27017';

MongoClient.connect(url, {useNewUrlParser: true}, (err, client) => {
    let db = client.db('TROC');
    assert.equal(null, err);

    app.get('/:collection/:type', (req, res) => {
        console.log('route: /' + req.params.collection + '/' + req.params.type);
        db.collection(req.params.collection)
            .find({'id':req.params.type}).toArray((err1, documents)=> {
            // la création de json ne sert à rien ici
                // on pourrait directement renvoyer documents
                let json = [];
                for (let doc of documents) {
                    console.log(doc);
                    json.push(doc);
                }
                res.setHeader('Content-type', 'application/json');
                res.end(JSON.stringify(json));
            });
    });

    app.get('/:collection', (req, res) => {
        db.collection(req.params.collection)
            .find({}).toArray((err2, documents)=> {
            // la création de json ne sert à rien ici
            // on pourrait directement renvoyer documents
                let json = [];
                for (let doc of documents) {
                    process.stdout.write(doc.email + '|');
                    json.push(doc.email);
                }
                res.setHeader('Content-type', 'application/json');
                res.end(JSON.stringify(json));
            });
    });

    // Recherche de bien ou de service avec date et mots clefs
    app.get('/Availability/:numWeek/:numDay', (req, res) => {
        db.collection('Availability').find(
            {'numWeek':req.params.numWeek, 'numDay':req.params.numDay}
        ).toArray((err2, documents)=> {
            res.setHeader('Content-type', 'application/json');
            res.end(JSON.stringify(documents));
        });
    });

    app.get('/', (req, res) => {
        console.log('route: /');
        res.send('Salut !');
    });
});

app.listen(8888);
